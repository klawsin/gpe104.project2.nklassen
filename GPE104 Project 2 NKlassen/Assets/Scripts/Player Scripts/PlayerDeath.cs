﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    public GameObject[] enemies;    //to access all the enemies

    //when player is destroyed, destroy all enemies
    void OnDestroy()
    {
        //put all enemies in an array
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        //delete all enemies in array with a for loop
        for (var i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i]);
        }
        //subtract players lives
        GameManager.instance.playerLives -= 1;
    }
}
