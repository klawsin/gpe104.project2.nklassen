﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    private Transform tf;           //access the transform component
    public float speed = 0.3f;      //speed of bullet

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //set the transform component
    }

    // Update is called once per frame
    void Update()
    {
        //Move the bullet forward
        tf.Translate(Vector3.up * speed, Space.Self);
    }
}
