﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootBullet : MonoBehaviour
{
    private Transform tf;               //access transform component
    public GameObject bulletPrefab;     //set the bullet prefab
    public float bulletLifeTime = 1f;   //set the bullet lifetime
    public GameObject bullet;           //for destroying bullet

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //set the transform component
    }

    // Update is called once per frame
    void Update()
    {
        //if space is pressed, fire bullet
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //spawn bullet
            bullet = Instantiate(bulletPrefab, tf.position, tf.rotation);
            //destroy bullet after a certain amount of time
            Destroy(bullet, bulletLifeTime);
        }
    }
}
