﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    private Transform tf; //assign transform component
    public float turnSpeed = 4f; //assign turn speed
    public float speed = 0.15f; //assign movement speed

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //Load out transform component into our variable
    }

    // Update is called once per frame
    void Update()
    {
        //if left arrow is pressed turn left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            tf.Rotate(0, 0, turnSpeed);
        }
        //if right arrow is pressed turn right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            tf.Rotate(0, 0, -turnSpeed);
        }
        //if up arrow is pressed go forward
        if (Input.GetKey(KeyCode.UpArrow))
        {
            tf.Translate(Vector3.up * speed, Space.Self);
        }

        //Delete player for testing purposes
        if (Input.GetKeyDown(KeyCode.R))
        {
            Destroy(this.gameObject);
        }
    }
}
