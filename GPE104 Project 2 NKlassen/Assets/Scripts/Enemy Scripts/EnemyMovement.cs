﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Transform tf;                   //accessing the transform component
    private GameObject player;              //accesing the player
    public float moveSpeed = 0.05f;         //how fast the enemy moves
    public float rotateSpeed = 3f;          //how fast the enemy rotates
    private Vector3 endVector;              //target destination
    private Vector3 startVector;            //beginning destination
    private Vector3 vectorBetweenPoints;    //between the two destinations
    private Vector3 moveVector;             //moving how fast we want to in the right direction

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();     //set the transform component
    }

    // Update is called once per frame
    void Update()
    {
        //If Player exists, set the player GameObject and set endVector to player's position
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            endVector = player.GetComponent<Transform>().position;
        }
        else //Else set our endVector to center screen
        {
            endVector = new Vector3(0, 0, 0);
        }

        //Set our starting vector
        startVector = tf.position;
        //Calculate the vector between our start and end vectors
        vectorBetweenPoints = endVector - startVector;
        //Change vector to magnitude of 1
        vectorBetweenPoints.Normalize();
        //Scale vector to match our speed
        moveVector = vectorBetweenPoints * moveSpeed;

        //If player exists, move and rotate towards it
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            //move
            tf.position = tf.position + moveVector;

            //rotate towards player
            Vector3 directionToLook = player.transform.position - tf.position;
            tf.up = directionToLook;
        }
    }
}
