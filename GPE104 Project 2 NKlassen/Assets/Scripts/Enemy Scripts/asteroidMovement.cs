﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidMovement : MonoBehaviour
{
    private Transform tf; //access the transform component
    private GameObject player; //for getting the player's vector
    public float rotateSpeed = 2f; //set the rotate speed of asteroid
    public float moveSpeed = 0.05f; //set the movement speed of asteroid
    private Vector3 endVector; //our target vector
    private Vector3 startVector; //our beginning vector
    private Vector3 vectorBetweenPoints; //finding the right angle to travel in
    private Vector3 moveVector; //moving our asteroid after being normalized and with the correct speed

    // Start is called before the first frame update
    void Start()
    {
        //Access transform component
        tf = GetComponent<Transform>();

        //If Player exists, set the player GameObject and set endVector to player's position
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            endVector = player.GetComponent<Transform>().position;
        }
        else //Else set our endVector to center screen
        {
            endVector = new Vector3(0, 0, 0);
        }
        
        //Set our starting vector
        startVector = tf.position;
        //Calculate the vector between our start and end vectors
        vectorBetweenPoints = endVector - startVector;
        //Change vector to magnitude of 1
        vectorBetweenPoints.Normalize();
        //Scale vector to match our speed
        moveVector = vectorBetweenPoints * moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        //Move towards player or center if player doesn't exist
        tf.position = tf.position + moveVector;

        //Rotate freely
        tf.Rotate(0, 0, rotateSpeed);
    }
}
