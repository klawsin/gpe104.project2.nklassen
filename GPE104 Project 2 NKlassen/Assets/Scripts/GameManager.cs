﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;     //for singleton pattern
    public GameObject playerPrefab;         //to create players
    public int playerLives = 3;             //player lives
    public List<GameObject> enemiesList;    //to access what enemies we want to spawn
    public List<GameObject> spawnPointList; //list of spawn points
    public GameObject[] spawnPoints;        //to see how many spawnpoints we have
    public GameObject[] enemies;            //to see how many enemies we have
    public GameObject spawnPointPrefab;     //to create the spawn points

    void Awake()
    {
        if (instance == null)
        {
            instance = this; //Store this instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); //Don't delete if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); //There can only be one game manager
            Debug.Log("Warning: A Second game manager was detected and destroyed.");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //If a player does not exist and we still have lives, create a player and subtract 1 from playerLives
        if (GameObject.FindGameObjectWithTag("Player") == null && playerLives > 0)
        {
            Instantiate(playerPrefab, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
        }

        //random spawn points
        //If less than 3 random spawnpoints, make them
        /*
        spawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
        if (spawnPoints.Length < 3)
        {
            Vector3 RandPosition = new Vector3(Random.Range(-10, 10), Random.Range(-5, 5), 0);
            Instantiate(spawnPointPrefab,RandPosition,Quaternion.identity);
        }
        */

        //if less than 3 enemies, spawn one
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        if (enemies.Length < 3)
        {
            int RandFromList = Random.Range(0, enemiesList.Count);
            int RandFromSpawns = Random.Range(0, spawnPointList.Count);
            Instantiate(enemiesList[RandFromList], spawnPointList[RandFromSpawns].transform.position, new Quaternion(0,0,0,0));
        }

        //if player is out of lives then end game
        if (playerLives <= 0)
        {
            Application.Quit();
        }
    }
}
